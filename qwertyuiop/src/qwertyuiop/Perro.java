/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qwertyuiop;
/**
 *
 * @author juanm
 */
public class Perro {
    String nombre;
    String raza;
    float peso;
    
    public Perro(){
        this.nombre = "Solovino";
        this.raza = "pedigri";
        this.peso = 7.2f;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }
}
